"""Tests for the endpoint module."""

from unittest.mock import MagicMock

import pytest

from pytda.auth import Auth
from pytda.api.method.get_quote import GetQuote


class TestGetQuote:
    """Test suite for GetQuote class."""

    @staticmethod
    @pytest.fixture
    def get_quote(mock_auth: Auth) -> GetQuote:
        """Create and return a `GetQuote` instance."""
        return GetQuote(mock_auth)

    @staticmethod
    def test_base_url(get_quote: GetQuote) -> None:
        """Test the base_url method returns the correct API base URL."""
        assert get_quote.base_url() == "https://api.tdameritrade.com/"

    @staticmethod
    def test_path(get_quote: GetQuote) -> None:
        """Test the path method returns the correct endpoint path."""
        assert get_quote.path() == "v1/marketdata/{symbol}/quotes"

    @staticmethod
    def test_get(get_quote: GetQuote, mock_get: MagicMock) -> None:
        """Test a get request is called with the correct parameters."""
        get_quote.get("abc")
        mock_get.assert_called_once_with(
            f"{get_quote.base_url()}{get_quote.path()}".format(symbol="abc"),
            headers={"Authorization": "Bearer secrettoken"},
            timeout=5,
        )
