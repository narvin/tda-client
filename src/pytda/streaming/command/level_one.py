"""Create actives requests for the TD Ameritrade streaming data."""

from typing import TypedDict, TypeGuard

from pytyu.json_schema import is_json_schema

from .base import BaseCommand
from .request import BaseRequest


class LevelOneRequestParameters(TypedDict):
    """Parameters for a level one request."""

    keys: str
    fields: str


class LevelOneRequest(BaseRequest):
    """Data for a level one request."""

    parameters: LevelOneRequestParameters


class LevelOne(BaseCommand):
    """Concrete class to create data for an level one request."""

    NON_LEVELONE_SERVICE_NAMES = ["OPTION", "QUOTE"]

    @staticmethod
    def is_level_one_request_data(request: object) -> TypeGuard[LevelOneRequest]:
        """Narrow `request` to `LevelOneRequest` type."""
        return is_json_schema(request, LevelOneRequest)

    def create(self, service: str, symbol: str, *fields: int) -> LevelOneRequest:
        """Return data to make an level one request."""
        real_service = (
            f"LEVELONE_{service}"
            if service.upper() not in self.NON_LEVELONE_SERVICE_NAMES
            else service
        )
        request = self._create(
            service=real_service.upper(),
            command="SUBS",
            parameters={
                "keys": symbol,
                "fields": ",".join(map(str, fields)),
            },
        )
        assert self.is_level_one_request_data(request)
        return request
