"""Async interfaces and classes for sending and receiving data."""

from abc import abstractmethod
from types import TracebackType
from typing import Optional, Protocol, Type

from websockets.client import connect, WebSocketClientProtocol


class IAsyncRecv(Protocol):  # pylint: disable=too-few-public-methods
    """Interface for a class that can receive a reqeust."""

    @abstractmethod
    async def recv(self) -> str:
        """Receive a request."""


class IAsyncSend(Protocol):  # pylint: disable=too-few-public-methods
    """Interface for a class that can send a reqeust."""

    @abstractmethod
    async def send(self, request: str) -> None:
        """Send a request."""


class WebSocketReceiverAndSender(IAsyncRecv, IAsyncSend):
    """Adapt a `WebSocketClientProtocol` to implement `IAsyncRecv` and `IAsyncSend`."""

    def __init__(self, conn: WebSocketClientProtocol) -> None:
        self.conn = conn

    async def send(self, request: str) -> None:
        await self.conn.send(request)

    async def recv(self) -> str:
        return str(await self.conn.recv())


class WebSocketConnectionManager:
    """Connection manager for a websockets connection."""

    def __init__(self, uri: str) -> None:
        self._uri = uri
        self.conn: Optional[WebSocketClientProtocol] = None

    async def __aenter__(self) -> WebSocketReceiverAndSender:
        self.conn = await connect(self._uri)
        if not self.conn.open:
            raise RuntimeError
        return WebSocketReceiverAndSender(self.conn)

    async def __aexit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_value: Optional[BaseException],
        exc_traceback: Optional[TracebackType],
    ) -> None:
        if self.conn is not None:
            await self.conn.close()
