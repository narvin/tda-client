"""Consume TD Ameritrade streaming data."""

from typing import Dict, List, TypedDict, TypeGuard

from pytyu.json_schema import is_json_schema


class BaseRequest(TypedDict):
    """Request base type."""

    service: str
    requestid: str
    command: str
    account: str
    source: str


class BaseRequestWithParameters(BaseRequest):
    """Request with parameters base type."""

    parameters: Dict[str, str]


class BaseRequests(TypedDict):
    """A collection of requests."""

    requests: List[BaseRequest | BaseRequestWithParameters]


def is_base_request(request: object) -> TypeGuard[BaseRequest]:
    """Narrow `request` to `BaseRequest` type."""
    return is_json_schema(request, BaseRequest)


def is_base_request_with_parameters(
    request: object,
) -> TypeGuard[BaseRequestWithParameters]:
    """Narrow `request` to `BaseRequestWithParameters` type."""
    return is_json_schema(request, BaseRequestWithParameters)
