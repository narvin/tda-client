"""Tests for the endpoint module."""

import sys
from unittest.mock import MagicMock

import pytest

from pytda.auth import Auth
from pytda.api.method.get_quotes import GetQuotes

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


class TestGetQuotes:
    """Test suite for GetQuotes class."""

    @staticmethod
    @pytest.fixture
    def get_quotes(mock_auth: Auth) -> GetQuotes:
        """Create and return a `GetQuotes` instance."""
        return GetQuotes(mock_auth)

    @staticmethod
    def test_base_url(get_quotes: GetQuotes) -> None:
        """Test the `base_url` method returns the correct API base URL."""
        assert get_quotes.base_url() == "https://api.tdameritrade.com/"

    @staticmethod
    def test_path(get_quotes: GetQuotes) -> None:
        """Test the `path` method returns the correct endpoint path."""
        assert get_quotes.path() == "v1/marketdata/quotes"

    @staticmethod
    @pytest.mark.parametrize(
        "symbols, query",
        [
            (("abc",), "symbol=abc"),
            (("abc", "def"), "symbol=abc%2Cdef"),
        ],
    )
    def test_get(
        symbols: Iterable[str], query: str, get_quotes: GetQuotes, mock_get: MagicMock
    ) -> None:
        """Test a get request is called with the correct parameters."""
        get_quotes.get(symbols)
        mock_get.assert_called_once_with(
            f"{get_quotes.base_url()}{get_quotes.path()}?{query}",
            headers={"Authorization": "Bearer secrettoken"},
            timeout=5,
        )
