"""
Sample app configuration

Add your own info to this file, then rename it to `config.py`
"""

CLIENT_ID = "ConsumerKey@AMER.OAUTHAP"  # app's consumer key + @AMER.OAUTHAP
REDIRECT_URI = "http://localhost"  # app's callback URL
