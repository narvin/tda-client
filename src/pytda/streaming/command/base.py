"""Consume TD Ameritrade streaming data."""

from abc import ABCMeta
from collections.abc import Mapping, MutableMapping
from itertools import count
from typing import Optional

from ...api.client import UserPrincipals
from .request import (
    BaseRequest,
    BaseRequestWithParameters,
    is_base_request,
    is_base_request_with_parameters,
)


class BaseCommand(metaclass=ABCMeta):  # pylint: disable=too-few-public-methods
    """Abstract base class to create request object."""

    request_id_iter = count(1)

    def __init__(self, user_principals: UserPrincipals) -> None:
        assert "accounts" in user_principals
        assert len(user_principals["accounts"]) > 0
        assert "streamerInfo" in user_principals
        assert user_principals["streamerInfo"] is not None
        self.user_principals = user_principals
        self.account = user_principals["accounts"][0]
        self.streamer_info = user_principals["streamerInfo"]

    def _create(
        self, service: str, command: str, parameters: Optional[Mapping[str, str]]
    ) -> BaseRequest | BaseRequestWithParameters:
        """Return request object."""
        request: MutableMapping[str, str | Mapping[str, str]] = {
            "service": service,
            "requestid": str(next(self.request_id_iter)),
            "command": command,
            "account": self.account["accountId"],
            "source": self.streamer_info["appId"],
        }
        if parameters is not None:
            request["parameters"] = parameters
        assert is_base_request(request) or is_base_request_with_parameters(request)
        return request
