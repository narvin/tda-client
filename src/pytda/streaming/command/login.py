"""Create login requests for the TD Ameritrade streaming data."""

from datetime import datetime, timezone
import json
from typing import TypedDict, TypeGuard
from urllib.parse import urlencode

from pytyu.json_schema import is_json_schema

from ..conn import IAsyncSend
from .base import BaseCommand
from .request import BaseRequest


class LoginRequestParameters(TypedDict):
    """Parameters for a login request."""

    credential: str
    token: str
    version: str


class LoginRequest(BaseRequest):
    """Data for a login request."""

    parameters: LoginRequestParameters


class Login(BaseCommand):
    """Concrete class to create data for a login request."""

    @staticmethod
    def is_login_request_data(request: object) -> TypeGuard[LoginRequest]:
        """Narrow `request` to `LoginRequest` type."""
        return is_json_schema(request, LoginRequest)

    async def send(self, asender: IAsyncSend) -> LoginRequest:
        """Return data to make a login request."""
        token_timestamp = self.streamer_info["tokenTimestamp"]
        token_dt = datetime.strptime(token_timestamp, "%Y-%m-%dT%H:%M:%S%z")
        epoch_dt = datetime.utcfromtimestamp(0).replace(tzinfo=timezone.utc)
        token_ms = int((token_dt - epoch_dt).total_seconds() * 1000)
        credential = {
            "userid": self.account["accountId"],
            "token": self.streamer_info["token"],
            "company": self.account["company"],
            "segment": self.account["segment"],
            "cddomain": self.account["accountCdDomainId"],
            "usergroup": self.streamer_info["userGroup"],
            "accesslevel": self.streamer_info["accessLevel"],
            "authorized": "Y",
            "timestamp": token_ms,
            "appid": self.streamer_info["appId"],
            "acl": self.streamer_info["acl"],
        }
        request = self._create(
            service="ADMIN",
            command="LOGIN",
            parameters={
                "credential": urlencode(credential),
                "token": self.streamer_info["token"],
                "version": "1.0",
            },
        )
        assert self.is_login_request_data(request)
        await asender.send(json.dumps(request))
        return request
