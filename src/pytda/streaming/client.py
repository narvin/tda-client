"""
Client to send async commands to the TD Ameritrade streaming data API and receive
responses.
"""

from __future__ import annotations
import asyncio
from collections.abc import Iterable, Mapping, MutableSequence
import json
from typing import Callable

from ..api.client import UserPrincipals
from .command.response import is_response
from .command.request import BaseRequest, BaseRequests
from .command.actives import Actives
from .command.level_one import LevelOne
from .command.login import Login
from .conn import IAsyncRecv, IAsyncSend, WebSocketConnectionManager

StreamingRecvProc = Callable[["Client", Mapping[str, object]], None]


class Client:  # pylint: disable=too-many-instance-attributes
    """Make streaming API requests and process responses."""

    def __init__(self, user_principals: UserPrincipals) -> None:
        assert "streamerInfo" in user_principals
        assert user_principals["streamerInfo"] is not None
        assert "streamerSocketUrl" in user_principals["streamerInfo"]
        assert user_principals["streamerInfo"]["streamerSocketUrl"] is not None
        self.user_principals = user_principals

        url = user_principals["streamerInfo"]["streamerSocketUrl"]
        uri = f"wss://{url}/ws"
        self.conn_mgr = WebSocketConnectionManager(uri)

        self._requests: BaseRequests = {"requests": []}
        self._logged_in = asyncio.Event()

        def login_proc(_: Client, resp_json: Mapping[str, object]) -> None:
            """Process received login responses."""
            resp_resps = resp_json.get("response")
            if not isinstance(resp_resps, Iterable):
                return
            for resp in resp_resps:
                if (
                    is_response(resp)
                    and resp["command"] == "LOGIN"
                    and resp["content"]["code"] == 0
                ):
                    self._logged_in.set()
                    return

        self._recv_processors: MutableSequence[StreamingRecvProc] = [login_proc]

        self.login = Login(user_principals).send
        self.actives = Actives(user_principals).create
        self.level_one = LevelOne(user_principals).create

    def add_request(self, request: BaseRequest) -> None:
        """Add a request to be sent."""
        self._requests["requests"] = [*self._requests["requests"], request]

    def append_recv_processor(self, processor: StreamingRecvProc) -> None:
        """Append a processor of received messages."""
        self._recv_processors.append(processor)

    async def send_requests(self, asender: IAsyncSend) -> None:
        """Send the requests in the queue."""
        await self._logged_in.wait()
        await asender.send(json.dumps(self._requests))
        self._requests["requests"] = []

    async def recv_forever(self, areceiver: IAsyncRecv) -> None:
        """Receive messages forever and pass them off to the processors."""
        while True:
            resp_text = str(await areceiver.recv())
            resp_json = json.loads(resp_text)
            for proc in self._recv_processors:
                proc(self, resp_json)
