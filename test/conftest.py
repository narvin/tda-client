"""pytest shared classes and fixtures."""

import sys
from typing import Callable, NamedTuple
from unittest.mock import MagicMock

import pytest
from pytest_mock.plugin import MockerFixture

if sys.version_info[:2] >= (3, 9):
    from collections.abc import Mapping
else:
    from typing import Mapping


class MockResponse(NamedTuple):
    """Mock `requests.Response` class."""

    status_code: int
    json: Callable[[], Mapping[str, object]]


@pytest.fixture
def mock_get(mocker: MockerFixture) -> MagicMock:
    """Mock `requests.get`."""
    mock = mocker.patch("requests.get", return_value=MockResponse(200, lambda: {}))
    return mock


@pytest.fixture
def mock_auth(mocker: MockerFixture) -> MagicMock:
    """Mock `pytda.auth.Auth` and return a mocked `Auth` instance."""
    mock_cls = mocker.patch("pytda.auth.Auth")
    mock_inst = mock_cls.return_value
    mock_inst.access_token = "secrettoken"
    assert isinstance(mock_inst, MagicMock)
    return mock_inst
