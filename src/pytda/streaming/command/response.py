"""Consume TD Ameritrade streaming data."""

from typing import Dict, List, Optional, TypedDict, TypeGuard

from pytyu.json_schema import is_json_schema


class ResponseContent(TypedDict):
    """Response content type."""

    code: int
    msg: str


class Response(TypedDict):
    """Response type."""

    service: str
    requestid: str
    command: str
    timestamp: int
    content: ResponseContent


class DataResponse(TypedDict):
    """Data response type."""

    service: str
    command: str
    timestamp: int
    content: List[Dict[str, str | int | float | bool]]


class NotifyResponse(TypedDict):
    """Notify response type."""

    heartbeat: str


class Responses(TypedDict):
    """An mapping of iterables of different response types."""

    response: Optional[List[Response]]
    data: Optional[List[DataResponse]]
    notify: Optional[List[NotifyResponse]]


def is_response(response: object) -> TypeGuard[Response]:
    """Narrow `response` to `Response` type."""
    return is_json_schema(response, Response)


def is_data_response(response: object) -> TypeGuard[DataResponse]:
    """Narrow `response` to `DataResponse` type."""
    return is_json_schema(response, DataResponse)


def is_notify_response(response: object) -> TypeGuard[NotifyResponse]:
    """Narrow `response` to `NotifyResponse` type."""
    return is_json_schema(response, NotifyResponse)


def is_responses(responses: object) -> TypeGuard[Responses]:
    """Narrow `responses` to `Responses` type."""
    return is_json_schema(responses, Responses)
