"""Create actives requests for the TD Ameritrade streaming data."""

from typing import Optional, TypedDict, TypeGuard

from pytyu.json_schema import is_json_schema

from .base import BaseCommand
from .request import BaseRequest


class ActivesRequestParameters(TypedDict):
    """Parameters for a actives request."""

    keys: str
    fields: str


class ActivesRequest(BaseRequest):
    """Data for a actives request."""

    parameters: ActivesRequestParameters


class Actives(BaseCommand):
    """Concrete class to create data for an actives request."""

    @staticmethod
    def is_actives_request_data(request: object) -> TypeGuard[ActivesRequest]:
        """Narrow `request` to `ActivesRequest` type."""
        return is_json_schema(request, ActivesRequest)

    def create(
        self, service: str, venue: Optional[str] = None, duration: int | str = 60
    ) -> ActivesRequest:
        """Return data to make an actives request."""
        real_venue = venue if venue is not None else service
        request = self._create(
            service=f"ACTIVES_{service.upper()}",
            command="SUBS",
            parameters={
                "keys": f"{real_venue.upper()}-{str(duration).upper()}",
                "fields": "0,1",
            },
        )
        assert self.is_actives_request_data(request)
        return request
