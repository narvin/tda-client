pytda
=====

Classes to easily access the TD Ameritrade API and Streaming Data. The project repo
also includes a console client, and a Flask web client that demonstrate how to use
the classes.
